---
name: Voice and tone
---

Copy and messaging is a core part of the experience of GitLab and the conversation with our users. The copy for GitLab is clear and direct. We strike a clear balance between professional and friendly. We can empathize with users (such as celebrating completing all To-Do items) and remain respectful of the importance of the work. We are a trusted and friendly coworker, helpful and understanding.

## Brevity

Users will skim content, rather than read text carefully. 

When familiar with a web app, users rely on muscle memory and may read even less when moving quickly. A good experience should quickly orient a user, regardless of their experience, to the purpose of the current screen. Understanding should happen without the user having to read long strings of text consciously.

In general, text is burdensome and adds cognitive load. This load is even more pronounced in a powerful productivity tool such as GitLab. We should not rely on words as a crutch to explain the purpose of a screen. The current navigation and composition of the elements on the screen should get the user 95% there, with the remaining 5% being specific elements such as text.

Copy should be concise and short whenever possible. A long message or label is a red flag hinting at a design that needs improvement. Preferably use context and placement of controls to make it obvious what clicking on them will do.
